# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 20:36:33 2023

@author: iturne01
"""

import pandas as pd
from ingestion import Ingestion

# Here is an example of the configuration used for ingestion.  It takes two different sets of JSON result files, one for total impacts
# and the other for upstream impacts, ingests them into DataFrames, then merges these DataFrames into a single combined DataFrame.
#
# You will need to adjust the "input_dir" field for each of the two file types.  You may also decide to use different fields.  We need
# to discuss the syntax of field selection in person at this point, if you want to do this.
#
dataframe_creation_list = [
    {
        "dataframe_name": "total_impacts_df",
        "creation_method": "read_from_json_files",
        "input_dir": "C:\\Users\\iturne01\\Desktop\\Automated Field crop trial\\Soybean oil\\",
        "file_selection_regex": "^Soybean oil production RU.*-result-total-impacts$",
        "columns": [
            {
                "column_name": "RU",
                "source": "from_filename",
                "spec": "^Soybean oil production RU(.*)-result-total-impacts$"
            },
            {
                "column_name": "impactCategory",
                "source": "from_json",
                "spec": "[].impactCategory.name"
            },
            {
                "column_name": "amount",
                "source": "from_json",
                "spec": "[].amount"
            },
            {
                "column_name": "refUnit",
                "source": "from_json",
                "spec": "[].impactCategory.refUnit"
            }
        ]
    },
    {
        "dataframe_name": "upstream_impacts_df",
        "creation_method": "read_from_json_files",
        "input_dir": "C:\\Users\\iturne01\\Desktop\\Automated Field crop trial\\Soybean oil\\",
        "file_selection_regex": "^Soybean oil production RU.*-result-upstream-of-impact-category$",
        "columns": [
            {
                "column_name": "RU",
                "source": "from_filename",
                "spec": "^Soybean oil production RU(.*)-result-upstream-of-impact-category$"
            },
            {
                "column_name": "impactCategory",
                "source": "from_json",
                "spec": "[].__keys__[0]"
            },
            {
                "column_name": "upstreamFlow",
                "source": "from_json",
                "spec": "[].__keys__[0].[0].upstreamTechFlows.[].techFlow.flow.name"
            },
            {
                "column_name": "upstreamFlowAmount",
                "source": "from_json",
                "spec": "[].__keys__[0].[0].upstreamTechFlows.[].result"
            }
        ]
    },
    {
        "dataframe_name": "combined_impacts_df",
        "creation_method": "merge",
        "left_dataframe": "total_impacts_df",
        "right_dataframe": "upstream_impacts_df",
        "left_merge_columns": [
            "RU",
            "impactCategory"
        ],
        "right_merge_columns": [
            "RU",
            "impactCategory"
        ]
    }
]
# After running this cell, your result will be in dataframes["combined_impacts_df"].  It is a Pandas DataFrame and
# you can analyze and visualize this as you wish.  Example visualizations are given in subsequent cells.

dataframes = Ingestion.create_dataframes(dataframe_creation_list)
len(dataframes["combined_impacts_df"])
combined_impacts_df = dataframes["combined_impacts_df"].copy()

combined_impacts_df

# combined_impacts_df.to_excel(excel_writer=r'C:\Users\iturne01\Desktop\Automated Field crop trial\Wheat.xlsx')

