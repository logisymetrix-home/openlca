# Logisymetrix-OpenLCA

January 10, 2025

## Description
The Logisymetrix-OpenLCA project currently contains:
* Automation code (includes the `olca__patched` package)
* Workbench

### Automation code
This reads Excel spreadsheets of a specific format and performs lifecycle analysis on them using OpenLCA 2.4.  It produces JSON output files, also of a specific format.

### Workbench
The workbench consists of a main Jupyter notebook and some supporting files.  This notebook can be used to do analysis and visualization of the JSON output from the automation step above.  It is liberally documented.  The nugget there is the flexible ingestion of JSON files into Pandas DataFrames.

## Installation and usage
Note that as of September, 2023, `Logisymetrix-OpenLCA` is supported only on Windows.

### Requirements
* Windows 10 or higher:
  * see https://www.howtogeek.com/197559/how-to-install-windows-10-on-your-pc/, for example
* Python 3.8.2 or higher:
  * use Anaconda which has Python bundled within it – see https://docs.anaconda.com/free/anaconda/install/windows/, for example
* OpenLCA 2.2.0 or higher
  * see https://www.openlca.org/download/
  * use the installer: openLCA_Windows_x64.exe
    * if you already have an older version of openLCA installed, uninstall it first
    * typically, this also means a DB backup and upgrade will be done when you start the new version

### Step-by-step installation instructions
Please follow the step-by-step description below each time there is a change to the Logisymetrix-OpenLCA project code:
* Back up your working folder `C:\path\to\work_root`, typically by copying it to `C:\path\to\work_root-<date>`
* Go to the __[UBC OpenLCA project](https://gitlab.com/logisymetrix-home/openlca)__ GitLab repository
* Click on the `Code` button, then choose `Download source code > zip`
* Move the zipped file from `Downloads` into your working folder `C:\path\to\work_root`
* Go to `C:\path\to\work_root` and unzip `openlca-main.zip` - this will produce the folder `openlca-main`
* Run Anaconda Navigator
* From Anaconda Navigator, launch Spyder
* Go into the Spyder console:
* Execute the following to find the location where you will install the new `olca__patched`:
```
import sys
print(sys.path)
```
* Look for a path that ends in `site-packages`, i.e.
```
'C:\\Users\\Myself\\AppData\\Local\\anaconda3\\Lib\\site-packages'
```
* In File Explorer, go to that folder (you will need to change `\\` to `\` if you copy-paste):
  * if there is already an `olca__patched` folder here, back it up, typically by `olca__patched-<date>`
  * in another File Explorer window, go to `C:\path\to\work_root\openlca-main\src`
  * copy the new `olca__patched` into the `site-packages` folder

### Automation code usage
#### Input Excel spreadsheet format
* Filename format is unrestricted:
  * however, if you have a sequence of files, it is highly recommended to use a standard naming format in order to enable parsing values from the filenames for analysis
* Currently the automation is hard-coded for conventional egg production - as such, each Excel file must contain 2 tabs:
  * manure
  * production
* This restriction will be removed in a later release:
  * however, the code could be modified by hand to accommodate other analyses
* The manure tab must contain these columns, in order:
  * `API Field`: ex. "Product flow 10"
  * `flow name (optional)`: ex. "Methane"
  * `UUID dataset`: UUID
  * `flow property`: ex. "Mass"
  * `unit`: ex. "kg"
  * `type`: ex. "PRODUCT_FLOW"
  * `is reference flow?`: "FALSE" or "TRUE"
  * `in/out`: "in" or "out"
  * `Conventional manure management`: float
  * `provider_UUID`: UUID
  * `Allocation factor`: float (optional)
* The production tab must contain these columns, in order:
  * `API Field`: ex. "Product flow 10"
  * `flow name (optional)`: ex. "Methane"
  * `UUID dataset`: UUID
  * `unit`: ex. "kg"
  * `in/out`: "in" or "out"
  * `Conventional egg production`: float
  * `provider_UUID`: UUID or "X" if this refers to the manure tab
  * `Allocation factor`: float (optional)
#### Step-by-step usage instructions
* Start OpenLCA:
  * right-click appropriate database > Open database
  * Tools > Developer tools > Console
  * Tools > Developer tools > IPC Server > click the `run` button
    * if you get an error here, it could be because you have something running on your machine at port 8080
    * see https://stackoverflow.com/questions/48198/how-do-i-find-out-which-process-is-listening-on-a-tcp-or-udp-port-on-windows
* Run Anaconda Navigator
* From Anaconda Navigator, launch Spyder
* In Spyder, open the automation code files:
  * File > Open... to open `Automation__1__out_of__2.py`
  * File > Open... to open `Automation__2__out_of__2.py`
* If necessary, edit the automation code to change the input and output file folders:
  * `Automation__1__out_of__2.py`:
    * change `INPUT_DIR` to be the folder containing the input spreadsheets
    * change the `farm_filenames` array to contain the spreadsheets you want to process
  * `Automation__2__out_of__2.py`:
    * change `OUTPUT_DIR` to be the directory where the output JSON files will be written
* Select `Automation__1__out_of__2.py` and Run > Run - for each Excel spreadsheet, this produces:
  * in OpenLCA: Navigation > `<database>` > Processes:
    * `Conventional egg production__<Excel spreadsheet name>`
    * `Conventional manure management__<Excel spreadsheet name>`
  * note that OpenLCA does not have an easy way to refresh the view of the DB, so you won't see these processes automatically
    * to verify they are there, you could do a search (search bar upper right)
  * files:
    * `production_processes.pkl` - cached results for Automation 2
    * `<OUTPUT_DIR>\<Excel spreadsheet name>-manure`
    * `<OUTPUT_DIR>\<Excel spreadsheet name>-production`
* Select `Automation__2__out_of__2.py` and Run > Run - for each process from step 1, this produces:
  * files:
    * `<OUTPUT_DIR>\Conventional egg production__<Excel spreadsheet name>-result-total-impacts`
    * `<OUTPUT_DIR>\Conventional egg production__<Excel spreadsheet name>-result-upstream-of-impact-category`
    * `<OUTPUT_DIR>\Conventional egg production__<Excel spreadsheet name>-result-total-flows`
    * `<OUTPUT_DIR>\Conventional egg production__<Excel spreadsheet name>-result-flows-of-impact-category`

### Workbench usage
The Logisymetrix-OpenLCA Workbench is written more generally and can work with any JSON files.  This release contains a Workbench Notebook that can be used with conventional egg production analysis as described here.
#### Step-by-step workbench usage instructions
* Run Anaconda Navigator
* launch JupyterLab
* within JupyterLab, navigate to `openlca-main\src\Workbench`
* double-click `ingest-elementary-flows-2024_01_14.ipynb`
* follow the directions therein

# Roadmap
These are the items left on the roadmap, as discussed to date:
1. ~~Add elementary flows to the automation code, currently there are only process flows~~ - 2024-01-21
1. Organize the existing elements into a proper data processing pipeline.  Generalization of data formats and processing steps is required.  For example, there is a need for items like:
    * data cleaning
    * data preparation
    * outlier identification and removal
    * statistical analysis
1. Connect the input of the OpenLCA processing pipeline to the Qualtrics API to access the Qualtrics survey format data.
