{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e1c1762-7651-4de5-9b2b-11a82a2108ea",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# September 7, 2023\n",
    "# This notebook is the baseline for the OpenLCA workbench project. With this you can do analysis of your\n",
    "# OpenLCA result files.\n",
    "#\n",
    "# Just run each cell of the notebook sequentially.\n",
    "#"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e43e83a6-3881-4681-81ee-eeba7aaf7dd0",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from ingestion import Ingestion"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e6b0184-a881-4e2d-99f2-1d9293655520",
   "metadata": {
    "tags": []
   },
   "source": [
    "## How-to configure ingestion\n",
    "You need to provide an array of DataFrame creation definitions.  They will be processed in order, thus subsequent definitions may refer to DataFrames created in precedent ones.  The result of the ingestion will be a dictionary of DataFrames, each one referred to by its dataframe_name.\n",
    "#### Single DataFrame creation definition\n",
    "Here are the fields in a DataFrame creation definition:\n",
    "* **dataframe_name** (string):\n",
    "    * the identifier of the created DataFrame, i.e. `dataframes[dataframe_name]`\n",
    "* **creation_method** (string):\n",
    "    * two methods are possible: `\"read_from_json_files\"` and `\"merge\"`\n",
    "    * these are distinctly different and the subsequent fields depend on the creation method\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1eb48ad2-1c97-424b-bed6-18929db24bce",
   "metadata": {
    "tags": []
   },
   "source": [
    "##### Read from JSON files (creation method)\n",
    "* **input_dir** (string):\n",
    "    * the directory which contains the JSON files to be processed - currently only a single directory is supported\n",
    "* **file_selection_regex** (string):\n",
    "    * see this __[Python regex tutorial](https://www.dataquest.io/blog/regular-expressions-data-scientists/)__ if you need explanation on working with regex\n",
    "    * in the example below, the regex matches total impact files for all housing types and all farm numbers\n",
    "    * regex is powerful, good to know it\n",
    "* **columns** (list):\n",
    "    * this is a list of column definitions for the DataFrame that is being created\n",
    "    * each column definition contains:\n",
    "        * **column_name** (string):\n",
    "            * this will become a DataFrame column name, so follow the appropriate syntax\n",
    "        * **source** (string):\n",
    "            * two sources are possible: `\"from_filename\"` and `\"from_json\"`:\n",
    "        * (from_filename) **spec** (string):\n",
    "            * this spec is a regex, like the file_selection_regex\n",
    "            * the difference is you must specify a group capture using `(` and `)`\n",
    "            * a reasonable tutorial on this is __[python-regex-capturing-groups](https://pynative.com/python-regex-capturing-groups/)__\n",
    "            * this column will have one value per file\n",
    "            * if other columns have multiple values per file, this value will be replicated as appropriate\n",
    "        * (from_json) **spec** (string):\n",
    "            * this is the most complex part of the definition\n",
    "            * the format of the from_json spec is based loosely on __[jmespath](https://jmespath.org/tutorial.html)__\n",
    "            * but be careful, it is a simplified version of it, so the documentation is only a rough guide\n",
    "            * the spec is a series of tokens separated by `.` that specifies a \"JSON path\"\n",
    "            * the position in the series corresponds to the depth in the JSON hierarchy\n",
    "            * the last token in the spec must correpond to a value or list of values\n",
    "            * briefly, the possible token values (or pairs) are:\n",
    "                * `<key>` - take only the sub-JSON or the value corresponding to this key\n",
    "                * `[].<key>` - take the full list of sub-JSON elements or values contained in the list at this level of the JSON\n",
    "                * `[n].<key>` - take the nth list element (sub-JSON or value)\n",
    "                * `__keys__[].<key>` - take the full list of sub-JSON elements or values contained at this level of the JSON\n",
    "                * `__keys__[n].<key>` - take the nth element (sub-JSON or value) at this level of the JSON\n",
    "            * in order to really understand this, please see __[Tutorial on using the from_json spec](test_ingest.ipynb#section_id1)__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a332f64-466d-4989-b43a-a9ed4f3e0897",
   "metadata": {},
   "source": [
    "##### Merge (creation method)\n",
    "* **left_dataframe** (string):\n",
    "    * the name of the \"left\" DataFrame to merge\n",
    "    * this is considered the primary DataFrame\n",
    "    * it must exist in the dictionary of DataFrames already created\n",
    "* **right_dataframe** (string):\n",
    "    * the name of the \"right\" DataFrame to merge\n",
    "    * this is considered the secondary DataFrame\n",
    "    * it must exist in the dictionary of DataFrames already created\n",
    "* **left_merge_columns** (list):\n",
    "    * list of columns in the left DataFrame to merge on\n",
    "* **right_merge_columns** (list):\n",
    "    * list of columns in the right DataFrame to merge on\n",
    "\n",
    "In order to really understand this, please see __[Tutorial on merging DataFrames](test_ingest.ipynb#section_id2)__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f96c2cdf-5fb2-4820-bdf6-4548a38ae178",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Here is an example of the configuration used for ingestion.  It takes two different sets of JSON result files, one for total impacts\n",
    "# and the other for upstream impacts, ingests them into DataFrames, then merges these DataFrames into a single combined DataFrame.\n",
    "#\n",
    "# You will need to adjust the \"input_dir\" field for each of the two file types.  You may also decide to use different fields.  We need\n",
    "# to discuss the syntax of field selection in person at this point, if you want to do this.\n",
    "#\n",
    "dataframe_creation_list = [\n",
    "    {\n",
    "        \"dataframe_name\": \"total_impacts_df\",\n",
    "        \"creation_method\": \"read_from_json_files\",\n",
    "        \"input_dir\": \"C:\\\\Users\\\\Grass_Valley\\\\Documents\\\\Projects\\\\FSPL (UBC)\\\\GitLab\\\\openlca\\\\resources\\\\Individual process data sets (processed)\\\\\",\n",
    "        \"file_selection_regex\": \"^Conventional egg production__Farm__.*__.*\\.xlsx-result-total-impacts$\",\n",
    "        \"columns\": [\n",
    "            {\n",
    "                \"column_name\": \"housingType\",\n",
    "                \"source\": \"from_filename\",\n",
    "                \"spec\": \"^Conventional egg production__Farm__(.*)__.*\\.xlsx-result-total-impacts$\",\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"farmNumber\",\n",
    "                \"source\": \"from_filename\",\n",
    "                \"spec\": \"^Conventional egg production__Farm__.*__(.*)\\.xlsx-result-total-impacts$\"\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"impactCategory\",\n",
    "                \"source\": \"from_json\",\n",
    "                \"spec\": \"[].impactCategory.name\"\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"amount\",\n",
    "                \"source\": \"from_json\",\n",
    "                \"spec\": \"[].amount\"\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"refUnit\",\n",
    "                \"source\": \"from_json\",\n",
    "                \"spec\": \"[].impactCategory.refUnit\"\n",
    "            }\n",
    "        ]\n",
    "    },\n",
    "    {\n",
    "        \"dataframe_name\": \"upstream_impacts_df\",\n",
    "        \"creation_method\": \"read_from_json_files\",\n",
    "        \"input_dir\": \"C:\\\\Users\\\\Grass_Valley\\\\Documents\\\\Projects\\\\FSPL (UBC)\\\\GitLab\\\\openlca\\\\resources\\\\Individual process data sets (processed)\\\\\",\n",
    "        \"file_selection_regex\": \"^Conventional egg production__Farm__(.*)__(.*)\\.xlsx-result-upstream-of-impact-category$\",\n",
    "        \"columns\": [\n",
    "            {\n",
    "                \"column_name\": \"housingType\",\n",
    "                \"source\": \"from_filename\",\n",
    "                \"spec\": \"^Conventional egg production__Farm__(.*)__.*\\.xlsx-result-upstream-of-impact-category$\",\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"farmNumber\",\n",
    "                \"source\": \"from_filename\",\n",
    "                \"spec\": \"^Conventional egg production__Farm__.*__(.*)\\.xlsx-result-upstream-of-impact-category$\"\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"impactCategory\",\n",
    "                \"source\": \"from_json\",\n",
    "                \"spec\": \"[].__keys__[0]\"\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"upstreamFlow\",\n",
    "                \"source\": \"from_json\",\n",
    "                \"spec\": \"[].__keys__[0].[0].upstreamTechFlows.[].techFlow.flow.name\"\n",
    "            },\n",
    "            {\n",
    "                \"column_name\": \"upstreamFlowAmount\",\n",
    "                \"source\": \"from_json\",\n",
    "                \"spec\": \"[].__keys__[0].[0].upstreamTechFlows.[].result\"\n",
    "            }\n",
    "        ]\n",
    "    },\n",
    "    {\n",
    "        \"dataframe_name\": \"combined_impacts_df\",\n",
    "        \"creation_method\": \"merge\",\n",
    "        \"left_dataframe\": \"total_impacts_df\",\n",
    "        \"right_dataframe\": \"upstream_impacts_df\",\n",
    "        \"left_merge_columns\": [\n",
    "            \"housingType\",\n",
    "            \"farmNumber\",\n",
    "            \"impactCategory\"\n",
    "        ],\n",
    "        \"right_merge_columns\": [\n",
    "            \"housingType\",\n",
    "            \"farmNumber\",\n",
    "            \"impactCategory\"\n",
    "        ]\n",
    "    }\n",
    "]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4c42efe-012a-40f6-9c47-aa84b3b4f60b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# After running this cell, your result will be in dataframes[\"combined_impacts_df\"].  It is a Pandas DataFrame and\n",
    "# you can analyze and visualize this as you wish.  Example visualizations are given in subsequent cells.\n",
    "\n",
    "dataframes = Ingestion.create_dataframes(dataframe_creation_list)\n",
    "dataframes[\"combined_impacts_df\"][0:20]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ca0428e-2f69-4e87-a76e-35bc54171f5e",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Sanity check on the length of the DataFrame, you should be able to predict this from your specification\n",
    "# in dataframe_creation_list.\n",
    "#\n",
    "len(dataframes[\"combined_impacts_df\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0ff184eb-5595-404c-93db-9228f94fedb0",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Extract the DataFrame of interest.\n",
    "#\n",
    "combined_impacts_df = dataframes[\"combined_impacts_df\"].copy()\n",
    "combined_impacts_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90c37695",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add an error column, equal to 0.1 * upstreamFlowAmount, for testing\n",
    "#\n",
    "combined_impacts_df[\"error\"] = 0.1 * combined_impacts_df[\"upstreamFlowAmount\"]\n",
    "combined_impacts_df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd44e154-9457-4450-965c-8b32957f3e7a",
   "metadata": {},
   "source": [
    "### Some DataFrame manipulation for analysis\n",
    "DataFrame manipulation is a bit tricky until you get used to it.  Here we will plot a stacked bar chart where:\n",
    "* the housing type is A\n",
    "* the impact category is Human toxicity - CML 2 baseline 2000\n",
    "* the impacts are stacked by upstream flow\n",
    "* there is one bar per farm\n",
    "* There is a single error bar at the top of each bar representing total uncertainty\n",
    "\n",
    "Let's look at this step by step."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92f38594-cb6e-4cbd-818c-33a426b38297",
   "metadata": {},
   "source": [
    "#### Filter on housing type\n",
    "*Note that in this example we are already filtered on housing type A because that was the only housing type represented in the input files.*\n",
    "\n",
    "Run the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37f9c633-25aa-4fde-aae5-85998a48c69f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "housing_type = \"A\"\n",
    "selected_impacts_df = combined_impacts_df.loc[combined_impacts_df[\"housingType\"] == housing_type]\n",
    "selected_impacts_df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88c578e2-172a-4917-a9f8-d1f5e7b60a15",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### Filter on impact category\n",
    "Run the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1fa3d4a1-e026-498c-9703-25110204fc27",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "impact_category = \"Human toxicity - CML 2 baseline 2000\"\n",
    "selected_impacts_df = selected_impacts_df.loc[selected_impacts_df[\"impactCategory\"] == impact_category]\n",
    "selected_impacts_df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71776970-6b9d-4434-8e5d-b80cd409366a",
   "metadata": {},
   "source": [
    "#### Get a list of upstream flows\n",
    "We will use this list to break the full DataFrame into multiple DataFrames, one per upstream flow.\n",
    "\n",
    "Run the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ecb588bb-e032-4b77-a529-bb676e9fe273",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "upstream_flow_list = selected_impacts_df[\"upstreamFlow\"].unique()\n",
    "upstream_flow_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a029542d-a1c7-466a-8e94-edf92fdbf6fc",
   "metadata": {},
   "source": [
    "#### Get information for labelling\n",
    "We are getting the ref_unit and bar tags.  In this case the bar tag is the farm number.  It is used for positioning and labelling the x-axis of the bar.\n",
    "\n",
    "*Note that we take a copy of the DataFrame and work with the copy.  In general, Pandas does not like to read and write a DataFrame at the same time for data consistency.  So, it is good practice to take a copy.  You'll get a warning whenever Pandas thinks there is a problem.*\n",
    "\n",
    "Run the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9d225dd-3bb3-4ff1-91c6-f657d3c43b6a",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "ref_unit = selected_impacts_df[\"refUnit\"].iloc[0]\n",
    "tagged_impacts_df = selected_impacts_df.copy()\n",
    "tagged_impacts_df[\"bar_tag\"] = selected_impacts_df[\"farmNumber\"]\n",
    "print(f\"ref_unit: {ref_unit}\")\n",
    "tagged_impacts_df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b27632b0-f9b3-4f62-abe1-23e357b31107",
   "metadata": {},
   "source": [
    "#### Split the full DataFrame by upstream flow\n",
    "Run the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b1f68b8-fdc5-4fdc-af7e-4543c07759f1",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "upstream_flow_dataframes = {}\n",
    "for upstream_flow in upstream_flow_list:\n",
    "    upstream_flow_dataframes[upstream_flow] = tagged_impacts_df.loc[tagged_impacts_df[\"upstreamFlow\"] == upstream_flow].reset_index()\n",
    "\n",
    "print(f\"There are now {len(upstream_flow_dataframes)} DataFrames:\")\n",
    "for key in upstream_flow_dataframes:\n",
    "    print(f\"  - {key}\")\n",
    "\n",
    "example_upstream_flow = 'Pullets-conventional'\n",
    "print(\" \")\n",
    "print(f\"For example, the {example_upstream_flow} DataFrame:\")\n",
    "upstream_flow_dataframes[example_upstream_flow]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d504b1b4",
   "metadata": {},
   "source": [
    "### Now the actual plotting\n",
    "**The following link is GOLD for examples of how to do plotting:**\n",
    "\n",
    ">__[matplotlib gallery](https://matplotlib.org/stable/gallery/index.html)__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dabcf4dc-f1d8-4102-85be-7ea35c6e3165",
   "metadata": {},
   "source": [
    "#### The stacked bar plot without a legend.\n",
    "Loop through the upstream flows, for each:\n",
    "* set the bottom of the bar segment to the top of the previous (0 for the first)\n",
    "* set the top of the bar segment to the bottom plus the upstream flow amount\n",
    "* add that bar segment to the plot, each bar segment added will have its own color (the legend will be dealt with later)\n",
    "* keep a running total of the error\n",
    "* for the last bar segment, plot the cumulative error bar\n",
    "\n",
    "Note that this is being done for all farms simultaneously.\n",
    "\n",
    "And, finally, do the labelling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "862e700b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is for the plot without the legend\n",
    "\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.axes import Axes\n",
    "\n",
    "cumval=None\n",
    "fig, ax = plt.subplots(figsize=(12,8))\n",
    "\n",
    "flow_count = len(upstream_flow_list)\n",
    "flow_index = 0\n",
    "for upstream_flow in upstream_flow_list:\n",
    "    print(upstream_flow)\n",
    "    upstream_flow_df = upstream_flow_dataframes[upstream_flow]\n",
    "    \n",
    "    if cumval is None:\n",
    "        cumval = pd.DataFrame()\n",
    "        cumval[\"bottom\"] = upstream_flow_df[\"upstreamFlowAmount\"] * 0\n",
    "        cumval[\"error\"] = upstream_flow_df[\"error\"]\n",
    "    if flow_index < flow_count - 1:\n",
    "        # for all flows but the last, plot bar without error\n",
    "        plt.bar(\n",
    "            x=upstream_flow_df[\"bar_tag\"], \n",
    "            height=upstream_flow_df[\"upstreamFlowAmount\"], \n",
    "            bottom=cumval[\"bottom\"],\n",
    "        )\n",
    "    else:\n",
    "        # for last flow, plot bar with error\n",
    "        plt.bar(\n",
    "            x=upstream_flow_df[\"bar_tag\"], \n",
    "            height=upstream_flow_df[\"upstreamFlowAmount\"], \n",
    "            bottom=cumval[\"bottom\"], \n",
    "            yerr=cumval[\"error\"], align='center', alpha=0.5, ecolor='gray', capsize=4\n",
    "        )\n",
    "\n",
    "    cumval[\"bottom\"] = cumval[\"bottom\"].add(upstream_flow_df[\"upstreamFlowAmount\"])\n",
    "    cumval[\"error\"] = cumval[\"error\"].add(upstream_flow_df[\"error\"])\n",
    "    flow_index += 1\n",
    "\n",
    "_ = plt.xticks(rotation=30)\n",
    "ax.set_xlabel(\"Farm number\")\n",
    "ax.set_ylabel(f\"{impact_category} ({ref_unit})\")\n",
    "ax.set_title(f\"Upstream impacts for housing type {housing_type} by farm number\")\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e7811df-9170-453f-9165-19f3a35513d1",
   "metadata": {},
   "source": [
    "#### The legend.\n",
    "This is done in a manner similar to the real plot.  It is just that the bar segments all have 0 height.  Also, the legend is exported as a JPEG to file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c859421d-04ff-4f2b-a8ad-d0c31d3f19b9",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# This is for a separate legend\n",
    "\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def export_legend(legend, filename=\"legend.png\"):\n",
    "    fig  = legend.figure\n",
    "    fig.canvas.draw()\n",
    "    bbox  = legend.get_window_extent().transformed(fig.dpi_scale_trans.inverted())\n",
    "    fig.savefig(filename, dpi=\"figure\", bbox_inches=bbox)\n",
    "\n",
    "cumval=None\n",
    "fig = plt.figure(figsize=(12,8))\n",
    "for upstream_flow in upstream_flow_list:\n",
    "    upstream_flow_df = upstream_flow_dataframes[upstream_flow]\n",
    "    if cumval is None:\n",
    "        cumval = pd.DataFrame()\n",
    "        cumval[\"bottom\"] = upstream_flow_df[\"upstreamFlowAmount\"] * 0\n",
    "    plt.bar(x=upstream_flow_df[\"bar_tag\"], height=cumval[\"bottom\"], bottom=cumval[\"bottom\"], label=upstream_flow)\n",
    "\n",
    "_ = plt.xticks(rotation=30)\n",
    "legend = plt.legend(fontsize=18)\n",
    "\n",
    "legend_filename=\"legend.png\"\n",
    "export_legend(legend, filename=legend_filename)\n",
    "print(f\"\\nThe legend without the bounding box is in the file: {legend_filename}\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e3c1f89b",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
