import olca__patched as olca
import pickle
# IM = 'IPCC 2013 GWP 100a'
IM = 'CML 2 baseline 2000'
OUTPUT_DIR = '/Users/jamesbamber/Documents/Projects/FSPL (UBC)/Bamber_Logisymetrix/Automation code'


client = olca.Client(8080)

# uuids of production processes
with open('production_processes.pkl', 'rb') as f:
    production_processes = pickle.load(f)

number_of_production_processes = len(production_processes)

#Create System Product
for i in range(number_of_production_processes):
#     curr_production_process_name = production_processes[i][0]
#     curr_production_process_uuid = production_processes[i][1]
#
#     print('client.create_product_system()')
#     client.create_product_system(
#         process_id         =  curr_production_process_uuid,
#         default_providers  =  'prefer',
#         preferred_type     =  'LCI_RESULT'
#     )
#
#
#
#     # Calculation
# #    setup = olca.CalculationSetup()
# #
# #    setup.calculation_type    =  olca.CalculationType.CONTRIBUTION_ANALYSIS
# #
# #    setup.allocation_method  =  olca.AllocationType.PHYSICAL_ALLOCATION
# #
# #    setup.impact_method      =  client.find(olca.ImpactMethod, IM)
# #
# #    setup.product_system     =  client.find(olca.ProductSystem, curr_production_process_name)
# #
# #    setup.amount             =  1.0
# #
# #    setup.withCosts          =  False
#
#     impact_method = client.find(olca.ImpactMethod, IM)
#     product_system = client.find(olca.ProductSystem, curr_production_process_name)
#
#     setup = olca.CalculationSetup(
# #        calculation_type   =  olca.CalculationType.CONTRIBUTION_ANALYSIS,
#         allocation_method  =  olca.AllocationType.PHYSICAL,
#         impact_method      =  impact_method,
#         product_system     =  product_system,
#         amount             =  1.0,
#         with_costs         =  False
#     )
#
#     print('client.calculate()')
#     resp, err = client.calculate(setup)
    resp = {"id": "1234"}
    curr_production_process_name = "Test_Farm_A__003"
    err = None
    if err is None:
        print('client.json_export()')
        client.json_export(resp, OUTPUT_DIR, curr_production_process_name)
