class single_Process_Creation():
    def __init__(self, single_farm_file_path, sheetName, manure_UUID):
        print('single_Process_Creation.__init__()')
        self.single_farm_file_path = single_farm_file_path
        self.sheetName = sheetName
        self.manure_UUID = manure_UUID

    def CreatE(self):
        print('single_Process_Creation.CreatE()')
        single_farm_file_path = self.single_farm_file_path
        sheetName = self.sheetName
        manure_UUID = self.manure_UUID
        
        
        # import olca__patched as olca
        import olca_ipc as olca
        import olca_schema
        import pandas as pd
        from datetime import datetime
        import uuid
        import math
        import os
        
        
        startIndex_records = 0
        
        # Conventional egg production
        Process_1_col_idx  = 8
        # Conventional manure management
        Process_2_col_idx  = 8
        
        
        
        client = olca.Client(8080)
        print(client)
        
        
        # data_import
        df_readout = pd.read_excel(single_farm_file_path, sheetName)

        # QuickUnitFix
        # TODO: this should be done in a general way
        # For now, we simply convert any kWh value to its value in MJ on the "production" sheet
        #   - this is done by multiplying by 3.6
        if self.sheetName == "production":
            df_readout.loc[df_readout['unit'] == "kWh", 'Conventional egg production'] *= 3.6
            df_readout.loc[df_readout['unit'] == "kWh", 'unit'] = 'MJ'

        """
        Creating new openLCA objects and linking imported data
        Finding corresponding flows and/or creating new flows
        """
        flow_properties = []
        # flow_units =[]
        is_ref_flow = []
        in_or_out = []
        allocation_factors = []
        flows = []
        
        
        dt_object = datetime.fromtimestamp(datetime.timestamp(datetime.now()))
        
        
        for index, row in df_readout.iloc[startIndex_records:].iterrows():
            print('**************************')
            print(index)
            
            cache = row['UUID dataset']
            
            
            #extracting information on flow property
            cur_flow_property = df_readout.loc[index, 'flow property']
            print('flow property = {}'.format(cur_flow_property))
            flow_property = client.find(olca_schema.FlowProperty, cur_flow_property)
            flow_properties.append(flow_property)

            # With OpenLCA 2.0, it is not so easy to set units on a FlowProperty, it is better to do a conversion to
            # the OpenLCA default units, see the fix above labelled "QuickUnitFix"
            #
            # #extracting information on unit
            # print('extracting information on unit')
            # cur_unit = df_readout.loc[index, 'unit']
            # print('unit = {}'.format(cur_unit))
            # print('client.find(olca_schema.Unit, cur_unit)')
            # unit = client.find(olca_schema.Unit, cur_unit)
            # flow_units.append(unit)
            
            print('extracting information on reference flow')
            cur_ref = df_readout.loc[index, 'is reference flow?']
            print('is reference flow? = {}'.format(cur_ref))
            is_ref_flow.append(cur_ref)
            
            print('extracting information on in/out')
            cur_in_or_out = df_readout.loc[index, 'in/out']
            print('in_or_out = {}'.format(cur_in_or_out))
            in_or_out.append(cur_in_or_out)
            
            print('extracting information on Allocation factor')
            cur_allocation_factor = df_readout.loc[index, 'Allocation factor']
            print('allocation_factor = {}'.format(cur_allocation_factor))
            allocation_factors.append(cur_allocation_factor)
            
            if cache == 'new_flow':      
                new_flow = olca_schema.Flow()
                new_flow.olca_type = 'Flow'
                new_flow.id = str(uuid.uuid4())      
                new_flow.flow_type = olca_schema.FlowType[df_readout.loc[index, 'type']]      
                new_flow.name = df_readout.loc[index, 'flow name']
                new_flow.description = 'Added as new flow from the olca-ipc python API on %s.\nImported file name: %s' % (dt_object, 'excel.io')
                
                flow_property_factor = olca_schema.FlowPropertyFactor()
                flow_property_factor.conversion_factor = 1.0
                flow_property_factor.flow_property = flow_property
                flow_property_factor.reference_flow_property = True
                
                new_flow.flow_properties = [flow_property_factor]
                
                client.insert(new_flow)
                
                cache = client.get(olca_schema.Flow, new_flow.id)
                flows.append(cache)
                
            # when the flow exists in db, it tries to find that flow by searching
            # its name in the db and finding its uuid.
            # second and thrid lines are the same as third if-cluse where uuid
            # is known
            elif cache == 'new_flow-multi_use':
                helper = client.find(olca_schema.Flow, df_readout.loc[index, 'flow name'])
                cache = client.get(olca_schema.Flow, helper.id)
                flows.append(cache)   
                
            else: # when uuid is known
                cur_UUID = row['UUID dataset']
                print('UUID = {}'.format(cur_UUID))
                
                cache = client.get(olca_schema.Flow, cur_UUID)
                flows.append(cache)
            
            print('------------------------')
        
            
        print('\033[1m' + 'list of imported flows:')
        
        imported_flows = []
        for i in range(len(flows)):
            imported_flows.append(flows[i].name)
        
        df_imported_flows = pd.DataFrame(list(zip(imported_flows,
                                                 in_or_out)),
                                        columns=['flow', 'in/out'])
        
        
        
        
        
        """
        Creating lists of exchanges for all (non-null) flows
        """
        
        
        ##how complicated! it is name of processes, i.e. hearder name of df
        process_columns = list(df_readout.iloc[:,Process_1_col_idx:Process_2_col_idx+1])  
        
        
        process_count = len(process_columns) #get max no of to import processes
        
        df_amounts = df_readout[process_columns].iloc[startIndex_records:]                       #create df with only flow amounts
        df_amounts.reset_index(drop=True, inplace=True)
        
        # provider
        df_provider = df_readout['provider_UUID'].iloc[startIndex_records:]                       #create df with only flow amounts
        df_provider.reset_index(drop=True, inplace=True)
        
        
        exchange_list = [[] for i in range(process_count)]
        
        for currentprocess in range(process_count):
            int_id_count = 1
            flow_count = 0
            
            for index, row in df_amounts.iloc[:, [currentprocess]].iterrows():
                
                new_exchange = olca_schema.Exchange()
                
                new_exchange.olca_type = 'Exchange'
                
                # set ref
                if is_ref_flow[index] == True:
                    new_exchange.quantitative_reference = True
                else:
                    new_exchange.quantitative_reference = False
                
                # input or output
                if in_or_out[index] == 'in':
                    new_exchange.input = True
                else:
                    new_exchange.input = False
                
                # amount    
                cur_amount = df_amounts.loc[index, process_columns[currentprocess]]
                cur_amount = float(cur_amount)
                new_exchange.amount = cur_amount
                
                # if new_exchange.amount == float(row[0]):
                #     print('the same')
                
                if math.isnan(new_exchange.amount) == True:
                    #print('Found a nan! :)')
                    flow_count += 1
                else:
                    new_exchange.internal_id = int_id_count
                    new_exchange.avoided_product = False
                    new_exchange.flow = flows[flow_count]
                    new_exchange.flow_property = flow_properties[flow_count]
#                    new_exchange.unit = flow_units[flow_count]
                    
                    # provider
                    curr_provider_UUID = df_provider[flow_count]
                    if isinstance(curr_provider_UUID, str) == True:
                        if curr_provider_UUID == 'X':
                            print(f"manure_UUID: {manure_UUID}")
                            new_exchange.default_provider = client.get(olca_schema.Process, manure_UUID)

                        else:                                
                            print(f"curr_provider_UUID: {curr_provider_UUID}")
                            new_exchange.default_provider = client.get(olca_schema.Process, curr_provider_UUID)
                        
                        
                    exchange_list[currentprocess].append(new_exchange)
        
                    int_id_count += 1
                    flow_count += 1
        
                    
        print('exchange lists successfully created for the following processes')
               
                
        # Creating new processes in olca database
        process_location = olca_schema.Location()
        process_documentation = olca_schema.ProcessDocumentation()
        
        importcounter = 0
        for column in df_readout[process_columns]:
            new_process = olca_schema.Process()
            new_process.olca_type = 'Process'
            
            new_UUID = str(uuid.uuid4())
            new_process.id = new_UUID
            
            new_process.process_type = olca_schema.ProcessType.UNIT_PROCESS

            new_process.name = column + '__' + os.path.basename(single_farm_file_path)
                
                
            new_process.description = '%s \nAdded as new process from the olca-ipc python API on %s. \nImported file name: %s' % (df_readout.loc[0, column], dt_object, 'excel.io')
            
            new_process.location = process_location
            
            new_process.location.id = df_readout.loc[1, column]
            
            if type(df_readout.loc[2, column]) == str and type(df_readout.loc[3, column]) == str:
                new_process.process_documentation = process_documentation
                new_process.process_documentation.valid_from = df_readout.loc[2, column]
                new_process.process_documentation.valid_until = df_readout.loc[3, column]
            else:
                pass 
                
                
            new_process.exchanges = exchange_list[importcounter]
            new_process.default_allocation_method = olca_schema.AllocationType.PHYSICAL_ALLOCATION
            allocation_factor_objects = []
            for i in range(len(flows)):
                flow = flows[i]
                allocation_factor = allocation_factors[i]
                print("{i}: allocation_factor={f}".format(i=i, f=allocation_factor))
                isnull = pd.isnull(allocation_factor)
                print("pd.isnull(allocation_factor) = {isnull}".format(isnull=isnull))
                if not isnull:
                    allocation_factor_object = olca_schema.new_physical_allocation_factor(new_process, flow, allocation_factor)
                    allocation_factor_objects.append(allocation_factor_object)

            new_process.allocation_factors = allocation_factor_objects
            importcounter += 1
            
            client.put(new_process)
            # client.update(new_process)
            print('\033[1m' + new_process.name, '\033[0m' +'succesfully imported to olca database')
            process_name = new_process.name

        return new_UUID, process_name
        
