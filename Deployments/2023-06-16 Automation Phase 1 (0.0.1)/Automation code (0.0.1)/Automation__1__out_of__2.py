from Process_Creation import single_Process_Creation
import pickle
import os

# MacOS style folder path
INPUT_DIR = '/Users/jamesbamber/Documents/Projects/FSPL (UBC)/Bamber_Logisymetrix/Individual process data sets'

# Windows style folder path
# INPUT_DIR = 'C:\\Users\\iturn\\OneDrive\\Documents\\2023-06-13 Automation Phase 1 (beta)\\Automation code'

farm_filenames = [
    "Farm__A__003.xlsx",
    "Farm__A__004.xlsx",
    # "Farm__A__007.xlsx",
    # "Farm__A__008.xlsx",
    # "Farm__A__009.xlsx",
    # "Farm__A__010.xlsx",
    # "Farm__A__012.xlsx",
    # "Farm__A__013.xlsx",
    # "Farm__A__015.xlsx",
    # "Farm__A__016.xlsx",
    # "Farm__A__018.xlsx",
    # "Farm__A__019.xlsx",
    # "Farm__A__020.xlsx",
    # "Farm__A__021.xlsx",
    # "Farm__A__022.xlsx",
    # "Farm__A__023.xlsx",
    # "Farm__A__024.xlsx",
    # "Farm__A__026.xlsx",
    # "Farm__A__028.xlsx",
    # "Farm__A__029.xlsx",
    # "Farm__A__030.xlsx",
    # "Farm__A__032.xlsx",
    # "Farm__A__033.xlsx",
    # "Farm__A__034.xlsx",
    # "Farm__A__037.xlsx",
    # "Farm__A__038.xlsx",
    # "Farm__A__039.xlsx",
    # "Farm__A__040.xlsx",
    # "Farm__A__041.xlsx",
    # "Farm__A__042.xlsx",
    # "Farm__A__044.xlsx",
    # "Farm__A__045.xlsx",
    # "Farm__A__046.xlsx",
    # "Farm__A__048.xlsx",
    # "Farm__A__049.xlsx",
    # "Farm__A__050.xlsx",
    # "Farm__A__052.xlsx",
    # "Farm__A__054.xlsx",
    # "Farm__A__056.xlsx",
    # "Farm__A__058.xlsx",
    # "Farm__A__059.xlsx",
    # "Farm__A__062.xlsx",
    # "Farm__A__065.xlsx",
    # "Farm__A__069.xlsx",
    # "Farm__A__070.xlsx",
    # "Farm__A__071.xlsx",
    # "Farm__A__074.xlsx",
    # "Farm__A__075.xlsx",
    # "Farm__A__077.xlsx",
    # "Farm__A__078.xlsx",
    # "Farm__A__079.xlsx",
    # "Farm__A__081.xlsx",
    # "Farm__A__082.xlsx",
    # "Farm__A__083.xlsx",
    # "Farm__A__084.xlsx",
    # "Farm__A__085.xlsx",
    # "Farm__A__086.xlsx",
    # "Farm__A__087.xlsx",
    # "Farm__A__088.xlsx",
    # "Farm__A__089.xlsx",
    # "Farm__A__090.xlsx",
    # "Farm__A__091.xlsx",
    # "Farm__A__095.xlsx",
    # "Farm__A__096.xlsx",
    # "Farm__A__098.xlsx",
    # "Farm__A__099.xlsx",
    # "Farm__A__100.xlsx",
    # "Farm__A__105.xlsx",
    # "Farm__A__106.xlsx",
    # "Farm__A__107.xlsx",
    # "Farm__A__109.xlsx",
    # "Farm__A__111.xlsx",
    # "Farm__A__112.xlsx",
    # "Farm__A__113.xlsx",
    # "Farm__A__114.xlsx",
    # "Farm__A__115.xlsx",
    # "Farm__A__116.xlsx",
    # "Farm__A__117.xlsx",
    # "Farm__A__119.xlsx",
    # "Farm__A__120.xlsx",
    # "Farm__A__121.xlsx",
    # "Farm__A__124.xlsx",
    # "Farm__A__125.xlsx",
    # "Farm__B__002.1.xlsx",
    # "Farm__B__002.xlsx",
    # "Farm__B__003.xlsx",
    # "Farm__B__004.xlsx",
    # "Farm__B__005.xlsx",
    # "Farm__B__006.xlsx",
    # "Farm__B__007.xlsx",
    # "Farm__B__008.xlsx",
    # "Farm__B__009.xlsx",
    # "Farm__B__010.xlsx",
    # "Farm__B__011.xlsx",
    # "Farm__B__012.xlsx",
    # "Farm__B__013.xlsx",
    # "Farm__B__015.xlsx",
    # "Farm__B__016.xlsx",
    # "Farm__B__020.xlsx",
    # "Farm__B__021.xlsx",
    # "Farm__B__023.xlsx",
    # "Farm__B__024.xlsx",
    # "Farm__B__026.xlsx",
    # "Farm__B__027.xlsx",
    # "Farm__B__028.xlsx",
    # "Farm__B__029.xlsx",
    # "Farm__B__030.xlsx",
    # "Farm__B__031.xlsx",
    # "Farm__B__033.xlsx",
    # "Farm__B__036.xlsx",
    # "Farm__B__037.xlsx",
    # "Farm__B__038.xlsx",
    # "Farm__B__041.xlsx",
    # "Farm__B__042.xlsx",
    # "Farm__B__043.xlsx",
    # "Farm__B__044.xlsx",
    # "Farm__B__046.xlsx",
    # "Farm__B__047.xlsx",
    # "Farm__B__049.xlsx",
    # "Farm__B__051.xlsx",
    # "Farm__B__052.xlsx",
    # "Farm__B__053.xlsx",
    # "Farm__C__001.xlsx",
    # "Farm__C__002.xlsx",
    # "Farm__C__003.xlsx",
    # "Farm__C__004.xlsx",
    # "Farm__C__006.xlsx",
    # "Farm__C__007.xlsx",
    # "Farm__C__008.xlsx",
    # "Farm__C__012.xlsx",
    # "Farm__D__006.xlsx",
    # "Farm__D__007.xlsx",
    # "Farm__D__008.xlsx",
    # "Farm__D__010.xlsx",
    # "Farm__D__013.xlsx",
    # "Farm__D__014.xlsx",
    # "Farm__E__001.xlsx",
    # "Farm__E__002.xlsx",
    # "Farm__E__004.xlsx",
    # "Farm__E__006.xlsx",
    # "Farm__E__008.xlsx",
    # "Farm__E__010.xlsx",
    # "Farm__E__012.xlsx",
    # "Farm__E__014.xlsx",
    # "Farm__E__015.xlsx",
    # "Farm__E__017.xlsx",
    # "Farm__E__018.xlsx",
    # "Farm__E__019.xlsx",
    # "Farm__E__020.xlsx",
    # "Farm__E__021.xlsx",
    # "Farm__E__022.xlsx",
    # "Farm__F__001.xlsx",
    # "Farm__F__002.xlsx",
    # "Farm__F__003.xlsx",
    # "Farm__F__004.xlsx",
    # "Farm__F__005.xlsx",
    # "Farm__F__007.xlsx",
    # "Farm__F__008.xlsx",
    # "Farm__F__009.xlsx",
]

print('Starting Automation__1__out_of__2')
production_processes = []

for single_farm_fileName in farm_filenames:
    single_farm_file_path = os.path.join(INPUT_DIR, single_farm_fileName)
    print('single_farm_file_path: {path}'.format(path=single_farm_file_path))

    try:
        curr_production_processes = []
        sheetName = 'manure'
        sp = single_Process_Creation(single_farm_file_path, sheetName, 'nothing')
        manure_UUID, manure_name = sp.CreatE()
        
        sheetName = 'production'
        sp = single_Process_Creation(single_farm_file_path, sheetName, manure_UUID)
        production_UUID, production_name = sp.CreatE()
        
        # summary
        curr_production_processes.append(production_name)
        curr_production_processes.append(production_UUID)
        
        production_processes.append(curr_production_processes)
    except FileNotFoundError:
        print('The file "{}" doesnt exist'.format(single_farm_fileName))
        continue

with open('production_processes.pkl','wb') as f:
    pickle.dump(production_processes, f)

