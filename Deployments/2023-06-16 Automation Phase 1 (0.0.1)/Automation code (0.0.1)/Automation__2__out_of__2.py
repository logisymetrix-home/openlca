import olca__patched as olca
import pickle
# IM = 'IPCC 2013 GWP 100a'
IM = 'CML 2 baseline 2000'

# MacOS style folder path
OUTPUT_DIR = '/Users/jamesbamber/Documents/Projects/FSPL (UBC)/Bamber_Logisymetrix/Automation code'

# Windows style folder path
# OUTPUT_DIR = 'C:\\Users\\iturn\\OneDrive\\Documents\\2023-06-13 Automation Phase 1 (beta)\\Automation code'


client = olca.Client(8080)

# uuids of production processes
with open('production_processes.pkl', 'rb') as f:
    production_processes = pickle.load(f)

number_of_production_processes = len(production_processes)

#Create System Product
for i in range(number_of_production_processes):
    curr_production_process_name = production_processes[i][0]
    curr_production_process_uuid = production_processes[i][1]
    print("{id}: {name}".format(id=curr_production_process_uuid, name=curr_production_process_name))
    
    print('client.create_product_system()')
    client.create_product_system(
        process_id         =  curr_production_process_uuid,
        default_providers  =  'prefer',
        preferred_type     =  'LCI_RESULT'
    )

    impact_method = client.find(olca.ImpactMethod, IM)
    product_system = client.find(olca.ProductSystem, curr_production_process_name)

    setup = olca.CalculationSetup(
#        calculation_type   =  olca.CalculationType.CONTRIBUTION_ANALYSIS,
        allocation_method  =  olca.AllocationType.PHYSICAL,
        impact_method      =  impact_method,
        product_system     =  product_system,
        amount             =  1.0,
        with_costs         =  False
    )

    print('client.calculate()')
    resp, err = client.calculate(setup)
    if err is None:
        result_id = resp.get('@id')
        print('client.json_export_result_detail("result/total-impacts")')
        data, err = client.json_export_result_detail("result/total-impacts", {"@id": result_id}, OUTPUT_DIR, curr_production_process_name)
        if err is None:
            params = {
                "@id": result_id,
                "totalImpacts": data
            }
            print('client.json_export_upstream_of_impact_category()')
            client.json_export_upstream_of_impact_category(params, OUTPUT_DIR, curr_production_process_name)
        client.dispose(resp)